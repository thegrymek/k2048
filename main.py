__author__ = 'thegrymek'

import kivy
kivy.require('1.8.0')
from kivy.app import App

from glob import glob
from os.path import dirname, join
from kivy.factory import Factory
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.properties import ObjectProperty
from kivy.core.window import Window
from kivy.utils import platform

from board import GameBoard
from tile import Tile

platform = platform()

class CustomPopup(Popup):
    game_board = ObjectProperty(None)


class GameScreen(Screen):
    pass

class SettingsScreen(Screen):

    game_board = ObjectProperty(None)

    def show_how_to_play_popup(self, *args):
        how_to_play_popup = Factory.get('HowToPlayPopup')
        p = how_to_play_popup()
        p.open()

    def show_popup_custom_board_size(self, *args):
        def call_resize_board(instance):
            width, height = instance.cwidth, instance.cheight
            board_width, board_height = self.game_board.board_width, self.game_board.board_height

            if width and height and [width, height] != [board_width, board_height]:
                self.game_board.callNewGame(width, height)
            self.parent.current = 'GameScreen'

        self.game_board = self.parent.ids['game_screen'].ids['game_board']
        CustomBoardResize = Factory.get('CustomBoardResize')
        p = CustomBoardResize(game_board=self.game_board)
        p.bind(on_dismiss=call_resize_board)
        p.open()


class App2028k(App):

    game_board = ObjectProperty(None)
    use_kivy_settings = False
    title = 'k2048'

    def build(self):
        if platform == "android":
            import android
            android.hide_keyboard()

    def _on_keyboard_settings(self, *args):
        return

    def open_settings(*args):
        pass

    def on_start(self):
        self.game_board = self.root.ids.game_screen.ids.game_board
        self.game_board.callStartGame()


if __name__ in ('__main__', '__android__'):
    app = App2028k()
    app.run()


