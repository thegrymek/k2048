'''
UserData
========

Manages historical data of game. Saves every move and points.
It holds high_score of game

'''
from datetime import datetime, date
from kivy.storage.jsonstore import JsonStore

class UserData(object):

    def sync(func):
        def inner(*args, **kwargs):
            result =  func(*args, **kwargs)
            args[0].sync_all()
            return result
        return inner

    def sync_all(self):
        if self.game:
            self.game.store_sync()
        self.userdata.store_sync()

    def __init__(self):
        self.userdata = JsonStore('userdata.json')

        if not self.userdata.exists('games'):
            self.userdata.store_put('games', [])

        if not self.userdata.exists('high_score'):
            self.userdata.store_put('high_score', 0)

        self.game = self.loadGame()


    @sync
    def newGame(self, board_width=4, board_height=4):
        game_file_name = '%s-%s.json' % (str(len(self.getGameFileNames())+1), date.today().isoformat())
        now = datetime.now().isoformat()
        game = JsonStore(game_file_name)
        game.store_put('created', now)
        game.store_put('states', {now: 'started'})
        game.store_put('board_width', board_width)
        game.store_put('board_height', board_height)
        game.store_put('moves', [])

        self.game = game
        self.addGameFileName(game_file_name)
        return self.game

    @sync
    def endGame(self):
        if self.game:
            self.addState('end')
            self.game.store_put('ended', datetime.now().isoformat())

    def loadGame(self, game_number=None):
        """ when game_number=None it loads last game
        """
        if game_number is None and hasattr(self, 'game') and self.game:
            return self.game

        if len(self.getGameFileNames()) == 0:
            return
        game_file_name = self.getGameFileNames()[game_number] if game_number else self.getGameFileNames()[-1]
        self.game = JsonStore(game_file_name)
        return self.game

    # --------------------------------------------------
    # High Score

    def getHighScore(self):
        return self.userdata.store_get('high_score')

    @sync
    def setHighScore(self, high_score):
        self.userdata.store_put('high_score', high_score)

    # --------------------------------------------------
    # Moves

    def getMoves(self):
        return self.game.get('moves')

    @sync
    def addMove(self, last_move):
        moves = self.getMoves()
        moves.append(last_move)
        self.game.store_put('moves', moves)

    @sync
    def deleteLastMove(self):
        self.game.store_put('moves', self.getMoves()[:-1])

    @sync
    def addState(self, state):
        now = datetime.now().isoformat()
        states = self.game.store_get('states')
        states[now] = state
        self.game.store_put('states', states)

    # --------------------------------------------------
    # Game File Names

    def getGameFileNames(self):
        return self.userdata.store_get('games')

    @sync
    def addGameFileName(self, file_name):
        game_files = self.userdata.store_get('games')
        game_files.append(file_name)
        self.userdata.store_put('games', game_files)
