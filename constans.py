from kivy.utils import get_color_from_hex

# ---------------------------------------------------------
# Icons

IMG_PATH = 'img/'
IMG_TILE_PATH = 'img/tiles/'

# ---------------------------------------------------------
# Userdata

USERDATA_PATH = 'data/userdata/'
USERDATA_GAMES_PATH = 'data/userdata/games/'
# ---------------------------------------------------------
# Directions

RIGHT = 'right'
LEFT = 'left'
TOP = 'top'
BOTTOM = 'bottom'

# ---------------------------------------------------------
# Board

BOARD_SIZE = 4

# Use below when Board_Size is None
BOARD_WIDTH = 4
BOARD_HEIGHT = 4

BOARD_BACKGROUND_COLOR = get_color_from_hex('d0ced0')
GAME_BACKGROUND_COLOR = get_color_from_hex('dbdbdb')