k2048
=======


[k2048][1] is a app based on game 2048 by [Gabriele Cirulli](http://gabrielecirulli.com/) 

Merge similar tiles and get to the 2024 tile!

To see menu press 'Enter' on desktop or 'menu' button on mobile.

[1]: https://bitbucket.org/thegrymek/k2048

Code
----
Clone [the repository][1] and run **python main.py**

Requirements
------------
* Python 2.7
* Kivy 1.9.0

Using bash or cmd:

    git clone https://thegrymek@bitbucket.org/thegrymek/k2048.git
    cd k2048
    python main.py

Supported operating systems
---------------------------
* Linux
* Windows
* Android

License
-------
MIT (see [LICENSE][2])

[2]: https://bitbucket.org/thegrymek/k2048/src/master/LICENSE
[3]: http://apache.org/licenses/LICENSE-2.0
