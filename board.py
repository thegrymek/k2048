'''
GameBoard
=========

Class responsible for display all board. It has tiles as buttons.
Row of the tiles is converted to class Row.
It is responsible for start and end game. Catches all movements and saves it in history.
Also updates points on the board.

'''
import random
import math

from kivy.vector import Vector
from kivy.metrics import dp
from kivy.uix.gridlayout import GridLayout
from kivy.properties import NumericProperty, StringProperty
from kivy.core.window import Window
from kivy.factory import Factory

from tile import Tile
from row import Row
from userdata import UserData
from constans import BOTTOM, TOP, RIGHT, LEFT

class GameBoard(GridLayout):

    score = NumericProperty(0)
    high_score = NumericProperty(0)

    board_width = NumericProperty(0)
    board_height = NumericProperty(0)

    last_direction = StringProperty('')

    def __str__(self):
        if isinstance(self.getTile(0,0), int):
            return ''

        s = 'Board:%dx%d' % (self.board_width, self.board_height) + '\n'
        s += 'Direction:' + self.last_direction + '\n'
        s += 'Score:'+str(self.score) + '\n'
        s += self.serializeBoard()
        return s

    def __init__(self, **kwargs):
        super(GameBoard, self).__init__(**kwargs)
        # default board 4x4
        self.board_width = 4
        self.board_height = 4
        self.rebuildBoard()

        # to store local data at the class scope
        self.userdata = {}

        # storage keeps saved status, files etc
        self.store = UserData()

        # Tells us whether tiles moved after player movement or not
        self.isMove = False

        # if platform() == 'android':
        #     import android
        #     import pygame
        #
        #     android.map_key(android.KEYCODE_MENU, 1000)
        #     android.map_key(android.KEYCODE_BACK, 1001)
        #     android.map_key(android.KEYCODE_HOME, 1002)
        #     android.map_key(android.KEYCODE_SEARCH, 1003)


        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down = self._on_keyboard_down)

    def init(self):
        self.updateHighScore(self.store.getHighScore())
        if not self.loadGame():
            self.store.newGame(self.board_width, self.board_height)
            self.addRandomTile()
            self.saveLastMove()

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down = self._on_keyboard_down)
        self._keyboard = None

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        key = keycode[1]
        if key == 'w' or key == 'up':
            self.doMovementToThe(TOP)
        if key == 'd' or key == 'right':
            self.doMovementToThe(RIGHT)
        if key == 's' or key == 'down':
            self.doMovementToThe(BOTTOM)
        if key == 'a' or key == 'left':
            self.doMovementToThe(LEFT)
        if key in (282, 319) or keycode[0] in (319,282) or key == 'enter':
            if self.screen_manager.current == 'GameScreen':
                self.callPause()
            else:
                self.callResume()


    # ---------------------------------------------------------
    # Board Features

    def loadGame(self, game_number=None):
        total_number_of_games = len(self.store.getGameFileNames())
        if total_number_of_games == 0:
            return False

        game = False
        if game_number in range(total_number_of_games):
            game = self.store.loadGame(game_number)
        else:
            game = self.store.loadGame()

        self.resizeBoard(cols=game.store_get('board_width') or 4, rows=game.store_get('board_height') or 4)
        if not game:
            return False

        last_move = self.getLastMove()
        if last_move:
            self.setBoard(last_move)

        return game

    def cleanBoard(self):
        [self.remove_widget(tile) for x,y, tile in self.iterate() if tile]

    def rebuildBoard(self):
        self._tiles = [[Tile(y,x) for x in range(self.board_width)] for y in range(self.board_height)]
        for i, j, t in self.iterate():
            self.add_widget(t)

    def resizeBoard(self, cols=None, rows=None):
        self.cleanBoard()
        self.clear_widgets()

        if rows:
            self.board_height = rows
        if cols:
            self.board_width = cols

        self.rows = self.board_height
        self.cols = self.board_width

        major = max(cols, rows)
        tile_size = self.parent.height / major if self.parent.width > self.parent.height else self.parent.width / major
        self.width = tile_size * cols
        self.height = tile_size * rows

        self.do_layout()

        self.rebuildBoard()

        last_move = self.getLastMove()
        if last_move:
            self.setBoard(last_move)

    def iterate(self):
        for i in range(self.board_height):
            for j in range(self.board_width):
                yield i, j, self._tiles[i][j]

    def getColumn(self, column):
        return Row([row[column] for row in self._tiles])

    def getRow(self, row):
        return Row(self._tiles[row])

    def getTile(self, x, y):
        return self._tiles[x][y]

    def setBoard(self, last_move):
        self.score = int(last_move['score'])
        #TODO refactor here
        self.deserializeBoard(last_move['board'])

    def getBoard(self):
        return {
            'board' : self.serializeBoard(),
            'score': self.score,
            'move': self.last_direction
        }

    def serializeBoard(self):
        return '\n'.join([str(self.getRow(row)) for row in range(self.board_height)])

    def deserializeBoard(self, serialized_board):
        rows = serialized_board.split('\n')
        for i in range(self.board_height):
            row_texts = rows[i].split('|')
            self.getRow(i).setTexts(row_texts)
            self.getRow(i).update()

    # ---------------------------------------------------------
    # Moves

    def setPreviousMove(self):
        self.store.deleteLastMove()
        last_move = self.getLastMove()
        if not last_move: return
        self.setBoard(last_move)
        print 'last_move'
        print self
        self.store.deleteLastMove()

    def getLastMove(self):
        return self.store.getMoves()[-1] if self.store.getMoves() else None

    def saveLastMove(self):
        last_move = self.getBoard()
        self.store.addMove(last_move)


    # ---------------------------------------------------------
    # States

    def callStartGame(self):
        self.screen_manager = self.parent.parent.parent.parent
        self.init()
        self.store.addState('started')

    def callNewGame(self, board_width=None, board_height=None):
        self.score = 0
        self.store.endGame()
        width = board_width or self.board_width
        height = board_height or self.board_height

        self.store.newGame(board_width=width, board_height=height)
        self.resizeBoard(width, height)
        self.addRandomTile()
        self.saveLastMove()

    def callPause(self):
        self.store.addState('paused')
        self.screen_manager.current = 'SettingsScreen'

    def callResume(self):
        self.store.addState('started')
        self.screen_manager.current = 'GameScreen'

    def callEndGame(self):
        EndGamePopup = Factory.get('EndGamePopup')
        EndGamePopup(game_board=self).open()
        print 'end game'

    def checkEndGame(self):
        if self.findEmptyTile():
            return False

        for i in range(self.board_height):
            row = self.getRow(i)
            if row.canMerge():
                return False

        for i in range(self.board_width):
            column = self.getColumn(i)
            if column.canMerge():
                return False

        return True

    def on_touch_up(self, touch):
        if not self.collide_point(touch.x, touch.y): return False

        v = Vector(touch.pos) - Vector(touch.opos)
        if v.length() < dp(20):
            return

        # detect direction
        dx, dy = v
        if abs(dx) > abs(dy):
            self.doMovementToThe(RIGHT if dx > 0 else LEFT)
        else:
            self.doMovementToThe(TOP if dy > 0 else BOTTOM)
        return True

    def getRowFromDirection(self, row_number, direction):
        """
        returns row from given number of row/column and given direction.
        always read tiles from right to left and from top to bottom
        because its easier to scale and push tiles
        that's why there is a list reverse
        """
        row = None
        if direction == RIGHT:
            row = self.getRow(row_number)[::-1]
        elif direction == LEFT:
            row = self.getRow(row_number)
        elif direction == TOP:
            row = self.getColumn(row_number)
        elif direction == BOTTOM:
            row = self.getColumn(row_number)[::-1]
        return Row(row)

    def doMovementToThe(self, direction):
        ''' its called when player do movement on the board '''
        row_length = self.board_height if direction in (RIGHT, LEFT) else self.board_width
        for i in range(row_length):
            # state of row before push & scale [ 2 | 2 | 2 | 2 ]
            row = self.getRowFromDirection(i, direction)
            original_texts = row.getTexts()
            row.push() # after push   [ 2 | 2 | 2 | 2 ]
            row.scale() # after scale [ 4 |   | 4 |   ]
            row.push() # after push   [ 4 | 4 |   |   ]
            row.update()
            if original_texts != row.getTexts():
                self.isMove = True
                score = self.countScore(original_texts, row.getTexts())
                self.updateScore(score)
                self.updateHighScore()

        tile = self.findEmptyTile()
        if tile is None:
            self.callEndGame()

        if self.isMove:
            self.last_direction = direction
            tile.Text = '2'
            tile.update()
            tile.animate()

            self.saveLastMove()
            self.saveHighScore()
            print self

        if self.checkEndGame():
            self.callEndGame()
        self.isMove = False

    def findEmptyTile(self):
        ''' finds and returns random tile from board.
        In case of no place returns None
        '''
        empty_tiles = [tile for x,y,tile in self.iterate() if not tile.Text]
        if not empty_tiles:
            return None
        return empty_tiles[random.randrange(0, len(empty_tiles))]

    def addRandomTile(self):
        tile = self.findEmptyTile()
        if not tile:
            return None
        tile.Text = '2'
        tile.update()
        tile.animate()
        return tile

    def countScore(self, original_texts, changed_texts):
        points = 0
        for i, text in enumerate(changed_texts):
            if not text: continue
            if text not in original_texts:
                points += int(text)
                continue
            if text in original_texts:
                original_texts.pop(original_texts.index(text))
        return points

    def updateScore(self, score):
        self.score += score

    def updateHighScore(self, new_high_score=None):
        if self.score > self.high_score:
            self.high_score = self.score
        if new_high_score:
            self.high_score = new_high_score

    def saveHighScore(self):
        self.store.setHighScore(self.high_score)

