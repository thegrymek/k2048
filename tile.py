'''
Tile
=======

Represents tile as packed button in boxlayout.
'''


from kivy.utils import get_color_from_hex
from kivy.uix.boxlayout import BoxLayout
from constans import IMG_TILE_PATH


class Tile(BoxLayout):

    colors = {
        0: 'light',
        2: 'yellow',
        4: 'green',
        8: 'green_dark',
        16: 'blue',
        32: 'light_yellow',
        64: 'light_green',
        128: 'violet',
        256: 'orange',
        512: 'red',
        1024: 'dark_blue',
        2048: 'dark_red',
        4096: 'dark'
    }

    def __init__(self,x, y, *args, **kwargs):
        super(Tile, self).__init__(*args, **kwargs)
        self.x = x
        self.y = y

    def __str__(self):
        try:
            return self.Text
        except KeyError:
            return ''

    def __repr__(self):
        try:
            return 'Tile %r %s' % (self.Text, id(self.Text))
        except KeyError:
            return 'Tile None'

    def on_press(self):
        print 'pressed'

    @property
    def Text(self):
        return self.ids['button'].text

    @Text.setter
    def Text(self, value):
        self.ids['button'].text = value

    def update(self):
        self.updatePicture()

    def updatePicture(self):
        value = int(self.Text) if self.Text else 0
        if value in self.colors.keys():
            self.setButtonColor(self.colors[value])
        else:
            self.setButtonColor('dark')

    def setButtonColor(self, color):
        if not self.ids.has_key('button'): return
        button = self.ids['button']
        button.background_normal = IMG_TILE_PATH + color + '.png'
        button.background_down = IMG_TILE_PATH + color + '.png'

    def animate(self):
        from kivy.animation import Animation
        # type = 'in_circ'
        # d = .2
        # animation = Animation(x=self.pos[0]-10, duration=d, t=type)
        # animation += Animation(x=self.pos[0]+10, duration=d, t=type)
        # animation += Animation(x=self.pos[0], duration=d, t=type)
        # animation.start(self)