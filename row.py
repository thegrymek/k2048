'''
Row
=======

Class helper for easier management of the current row at the board.
'''

# Helper class for better management of tiles in board
class Row(object):

    def __init__(self, tiles):
        self.row = tiles
        self.tile_texts = [tile.Text for tile in self.row]

    def __getitem__(self, item):
        return self.row[item]

    def __len__(self):
        return len(self.row)

    def __str__(self):
        return '|'.join([t if t else '0' for t in self.tile_texts])

    def __iter__(self):
        return self.row.__iter__()

    def setTexts(self, texts):
        if len(texts) != len(self.row): return
        for i, tile in enumerate(self.row):
            tile.Text = texts[i] if texts[i] != '0' else ''

    def getTexts(self):
        return [tile.Text for tile in self.row]

    def setRow(self, row):
        for i, tile in enumerate(self.row):
            tile.Text = row.tile_texts[i]

    def update(self):
        self.updateTexts()
        self.updateTiles()

    def updateTexts(self):
        self.tile_texts = [tile.Text for tile in self.row]

    def updateTiles(self):
        [tile.update() for tile in self.row]

    def isFilled(self):
        return all(self.tile_texts)

    def isEmpty(self):
        return not any(self.tile_texts)

    def canMerge(self):
        texts = filter(None, self.getTexts())
        if texts == list(set(texts)):
            return False
        return True

    def push(self):
        if self.isEmpty() or self.isFilled():
            return
        empty_tiles = [tile for tile in self.row if not tile.Text]
        tiles = [tile for tile in self.row if tile.Text]
        pushed_row = Row(tiles + empty_tiles)
        self.setRow(pushed_row)

    def scale(self):
        if self.isEmpty():
            return
        for i in range(len(self.row)-1):
            tile1, tile2 = self.row[i:i+2]
            text1, text2 = tile1.Text, tile2.Text
            if text1 and text1 == text2:
                tile1.Text = str(int(text1) + int(text2))
                tile2.Text = ''
        self.updateTexts()
